<?php

namespace Dennyvik\Dvpack01;

use Illuminate\Support\Facades\Facade;

class DemoFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'dennyvik-demo';
    }
}