<?php

namespace Dennyvik\Dvpack01;

use App\Providers\AppServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class Dvpack01ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        require __DIR__.'/routes/routes.php';

        $this->loadViewsFrom(__DIR__ . '/views', 'dennyvik-dvpack01');
        View::addNamespace('LAYOUTS', __DIR__ . '/views/layouts');
        View::addNamespace('TEMPLATES', __DIR__ . '/views/templates');

        $this->publishes([
            __DIR__ . '/config' => config_path('dennyvik-dvpack01-demo'),
        ]);

        $this->publishes([
            __DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
        ], 'migrations');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind('dennyvik-demo', function() {
            return new Demo();
        });

        $this->mergeConfigFrom(
            __DIR__ . '/config/main.php', 'dennyvik-dvpack01-demo'
        );

    }
}
