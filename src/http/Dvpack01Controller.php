<?php

namespace Dennyvik\Dvpack01;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Dvpack01Controller extends BaseController
{
    //

    public function demo(){
        return \Demo::hello() . ' from controller.';

    }
    public function index(){
        return view('dennyvik-dvpack01::home',['title'=>"DVPACK01 Home Page"]);
    }
    public function lists(){
        return view('dennyvik-dvpack01::list',['title'=>"DVPACK01 Data Page"]);
    }
    public function view(){
        return view('dennyvik-dvpack01::view',['title'=>"DVPACK01 View Detail Page"]);
    }
    public function create(){
        return view('dennyvik-dvpack01::create',['title'=>"DVPACK01 Add Data Page"]);
    }
    public function update(){
        return view('dennyvik-dvpack01::update',['title'=>"DVPACK01 Update Data Page"]);
    }
}
