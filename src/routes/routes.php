<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('demo/test', function () {
    return 'Test';
});

Route::get('demo', 'Dennyvik\Dvpack01\Dvpack01Controller@demo');

Route::get('demo/config', function () {
    return config('dennyvik-dvpack01-demo.hello') .
        config('dennyvik-dvpack01-demo.world');
});
Route::get('demo/model', function () {
    echo "Data from Database : <br /><br />";
    foreach(\Dennyvik\Dvpack01\Models\Item::get() as $item){
        echo "> " . $item->id." | ".$item->slug." | ".$item->name." | ".$item->description." <br />";
    };
});

Route::group(['prefix'=>'dvpack01'],function(){
    Route::get('/', 'Dennyvik\Dvpack01\Dvpack01Controller@index')->name('dvpack01.home');
    Route::get('/list', 'Dennyvik\Dvpack01\Dvpack01Controller@lists' )->name('dvpack01.list');
    Route::get('/add', 'Dennyvik\Dvpack01\Dvpack01Controller@create' )->name('dvpack01.create');
    Route::get('/view', 'Dennyvik\Dvpack01\Dvpack01Controller@view' )->name('dvpack01.view');
    Route::get('/edit', 'Dennyvik\Dvpack01\Dvpack01Controller@update' )->name('dvpack01.update');
    Route::get('/demo/hello', function () {
        return Demo::hello();
    });
});
