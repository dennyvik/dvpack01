<?php

namespace Dennyvik\Dvpack01\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'dennyvik_dvpack01_demo_items';
}