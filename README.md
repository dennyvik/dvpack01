# README #

This Package is for Personal Study only.

This package have routes, controllers and view inside it's 'Vendor' Folder ('vendor\dennyvik\dvpack01\').


### What is this repository for? ###

* For Laravel training on Topic : Packages
* Version : dev-master

### How do I get set up? ###

* Follow this step-by-step

    - require this Package in your composer 
        
            "require-dev": {
                    ***
                    "dennyvik\dvpack01" : "dev-master",
                    ***
            }
    - run in console 
                
            > composer update        
        
* Configuration

    ###### composer.json ######
    
    * add this into your Laravel project under autoload
    
            "autoload": {
                    "classmap": [
                        "vendor/dennyvik/dvpack01/src/migrations"
                    ],
                    "psr-4": {
                        "Dennyvik\\Dvpack01\\":"vendor/dennyvik/dvpack01/src"
                    }
                }
        
    * run in console 
                
            > composer update
    
    ###### config\app.php ######
    
            'providers' => [
                        ***
                            
                        Dennyvik\Dvpack01\Dvpack01ServiceProvider::class,
                        
                        ***
                ],
                
                'aliases' => [
                        ***
                        
                        'Demo' => Dennyvik\Dvpack01\DemoFacade::class,
                        
                        ***
                ]
        
    ###### Console ######
    
    Run this in the console 
        
        >php artisan vendor:publish
    
    
* Dependencies 

      "illuminate/support": "~5"
            
        
* Database configuration

    Sample data Seeds provided in the vendors folder, you can do Seeding to your database :
         
         > php artisan db:seed --class=Dennyvik\Dvpack01\DemoSeeder
    
* If Success

    Consider all running well, in this package there are some routes you can access directly from the browser's URL :
            
            http://localhost:8000/demo //Displaying from controller
            http://localhost:8000/demo/test //Displaying directly from Routes.php
            http://localhost:8000/demo/config //Displaying Config's parameter data
            http://localhost:8000/demo/model //Displaying data from DB
            
            http://localhost:8000/dvpack01 //which is a group route consist of many sub-routes. From here you can explore in the sourcecode to learn about Blade Template's Extend and Include. 
            
            // Localhost and Port should be match with your machine environtment
        
* Deployment instructions

        * NOT FOR DEPLOYMENT !* 
    



